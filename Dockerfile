FROM balenalib/raspberrypi4-64-debian:buster-run

RUN install_packages dnsmasq wireless-tools wget

WORKDIR /usr/src/app

ARG WIFI_CONNECT_VERSION=4.4.6
ARG TARGETOS
ARG TARGETARCH=aarch64

RUN wget -cO- "https://github.com/balena-io/wifi-connect/releases/download/v$WIFI_CONNECT_VERSION/wifi-connect-v$WIFI_CONNECT_VERSION-$TARGETOS-$TARGETARCH.tar.gz" \
  | tar -xvz -C .

COPY start.sh /

CMD ["bash", "/start.sh"]
